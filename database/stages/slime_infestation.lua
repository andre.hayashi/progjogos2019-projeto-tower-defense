
return {
  title = 'Slime Infestation',
  waves = {
    { green_slime = 10 },
    { green_slime = 20 },
    { blue_slime = 5, green_slime = 30 },
  }
}

